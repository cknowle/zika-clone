<?php

/**
 * @file
 * Contains \Drupal\migrate_wordpress\Plugin\migrate\source\Posts.
 */

namespace Drupal\migrate_wordpress\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Row;

/**
 * @MigrateSource(
 *   id = "wp_posts"
 * )
 */
class Posts extends SqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Prepare sub-queries for taxonomy terms in different vocabs
    $language_query = '(SELECT GROUP_CONCAT(wt.name SEPARATOR \', \') FROM  wp_zcn.' . $this->configuration['table_prefix'] . 'term_relationships wtr INNER JOIN  wp_zcn.' . $this->configuration['table_prefix'] . 'term_taxonomy wtt ON wtr.term_taxonomy_id = wtt.term_taxonomy_id INNER JOIN  wp_zcn.' . $this->configuration['table_prefix'] . 'terms wt ON wtt.term_id = wt.term_id WHERE wtr.object_id = p.id AND wtt.taxonomy = "language" GROUP BY wtr.object_id)';

    $audience_query = '(SELECT GROUP_CONCAT(wt.name SEPARATOR \', \') FROM  wp_zcn.' . $this->configuration['table_prefix'] . 'term_relationships wtr INNER JOIN  wp_zcn.' . $this->configuration['table_prefix'] . 'term_taxonomy wtt ON wtr.term_taxonomy_id = wtt.term_taxonomy_id INNER JOIN  wp_zcn.' . $this->configuration['table_prefix'] . 'terms wt ON wtt.term_id = wt.term_id WHERE wtr.object_id = p.id AND wtt.taxonomy = "audience" GROUP BY wtr.object_id)';

    $source_query = '(SELECT GROUP_CONCAT(wt.name SEPARATOR \', \') FROM  wp_zcn.' . $this->configuration['table_prefix'] . 'term_relationships wtr INNER JOIN  wp_zcn.' . $this->configuration['table_prefix'] . 'term_taxonomy wtt ON wtr.term_taxonomy_id = wtt.term_taxonomy_id INNER JOIN  wp_zcn.' . $this->configuration['table_prefix'] . 'terms wt ON wtt.term_id = wt.term_id WHERE wtr.object_id = p.id AND wtt.taxonomy = "source" GROUP BY wtr.object_id)';

    $topic_query = '(SELECT GROUP_CONCAT(wt.name SEPARATOR \', \') FROM  wp_zcn.' . $this->configuration['table_prefix'] . 'term_relationships wtr INNER JOIN  wp_zcn.' . $this->configuration['table_prefix'] . 'term_taxonomy wtt ON wtr.term_taxonomy_id = wtt.term_taxonomy_id INNER JOIN  wp_zcn.' . $this->configuration['table_prefix'] . 'terms wt ON wtt.term_id = wt.term_id WHERE wtr.object_id = p.id AND wtt.taxonomy = "topic" GROUP BY wtr.object_id)';

    $country_query = '(SELECT GROUP_CONCAT(wt.name SEPARATOR \', \') FROM  wp_zcn.' . $this->configuration['table_prefix'] . 'term_relationships wtr INNER JOIN  wp_zcn.' . $this->configuration['table_prefix'] . 'term_taxonomy wtt ON wtr.term_taxonomy_id = wtt.term_taxonomy_id INNER JOIN  wp_zcn.' . $this->configuration['table_prefix'] . 'terms wt ON wtt.term_id = wt.term_id WHERE wtr.object_id = p.id AND wtt.taxonomy = "country" GROUP BY wtr.object_id)';

    $resource_query = '(SELECT GROUP_CONCAT(wt.name SEPARATOR \', \') FROM  wp_zcn.' . $this->configuration['table_prefix'] . 'term_relationships wtr INNER JOIN  wp_zcn.' . $this->configuration['table_prefix'] . 'term_taxonomy wtt ON wtr.term_taxonomy_id = wtt.term_taxonomy_id INNER JOIN  wp_zcn.' . $this->configuration['table_prefix'] . 'terms wt ON wtt.term_id = wt.term_id WHERE wtr.object_id = p.id AND wtt.taxonomy = "type_of_resource" GROUP BY wtr.object_id)';

    // Get all the posts, post_type=post filters out revisions and pages.
    $query = $this->select($this->configuration['table_prefix'] . 'posts', 'p')
      ->fields('p', array_keys($this->postFields()))
      ->condition('post_type', 'zikacomresource')
      ->condition('post_status', ['publish', 'private', 'draft'], 'IN')
      ->orderBy('post_date', 'ASC');
/*
    $query->addExpression($language_query, 'LanguageTerms');
    $query->addExpression($audience_query, 'AudienceTerms');
    $query->addExpression($source_query, 'SourceTerms');
    $query->addExpression($topic_query, 'TopicTerms');
    $query->addExpression($country_query, 'CountryTerms');
    $query->addExpression($resource_query, 'ResourceTypeTerms');
*/

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $row->setSourceProperty('post_date', strtotime($row->getSourceProperty('post_date')));
    $row->setSourceProperty('post_modified', strtotime($row->getSourceProperty('post_modified')));

    // This gathers up WordPress tags and categories and applies it to the hard
    // code field name field_tags in migrate.migration. ' . $this->configuration['table_prefix'] . 'posts.yml. Use
    // hook_migration_load() to update the field name or simply copy the
    // migration.
//    $this->applyCustomTagsFieldMapping($row);
    $this->applyCustomTagsFieldMapping($row, 'field_language', 'language');
    $this->applyCustomTagsFieldMapping($row, 'field_audience', 'audience');
    $this->applyCustomTagsFieldMapping($row, 'field_source_organization', 'source_organization');
    $this->applyCustomTagsFieldMapping($row, 'field_topic', 'topic');
    $this->applyCustomTagsFieldMapping($row, 'field_country', 'country');
    $this->applyCustomTagsFieldMapping($row, 'field_type_of_resource', 'type_of_resource');
    $this->applyImgMapping($row);

    return parent::prepareRow($row);
  }

  /**
   * Apply custom mapping logic for taxonomy reference fields.
   *
   * @param \Drupal\migrate\Row $row
   *   The row object.
   *
   * @throws \Exception
   */
  protected function applyCustomTagsFieldMapping(Row $row, $field_name, $topic) {
    $wp_topic = ($topic == 'source_organization') ? 'source' : $topic;
//    \Drupal::logger('migrate_wordpress')->notice('term topic: @topic, and current source id: %id', array('@topic' => $topic, '%id' => $row->getSourceProperty('id')));
    $query = $this->select($this->configuration['table_prefix'] . 'term_relationships', 'tr')
//      ->fields('tt', ['term_id']);
      ->fields('t', array('name'));
    $query
      ->condition('object_id', $row->getSourceProperty('id'));
    $query->join($this->configuration['table_prefix'] . 'term_taxonomy', 'tt', 'tt.term_taxonomy_id = tr.term_taxonomy_id');
    $query->join($this->configuration['table_prefix'] . 'terms', 't', 'tt.term_id = t.term_id');
    $query->condition('tt.taxonomy', $wp_topic);
//    $query->addExpression('GROUP_CONCAT(t.name)', $topic . 'Terms');

//    \Drupal::logger('migrate_wordpress')->notice('Term query: @query', array('@query' => dpq($query)));
    $results = $query->execute()->fetchCol();

    $tags = [];
    $previous_ids = [];
//    \Drupal::logger('migrate_wordpress')->notice('Inside processing of @topic terms', array('@topic' => $topic));
//    \Drupal::logger('migrate_wordpress')->notice('Results: @results', array('@results' => implode('|', $results)));
    foreach ($results as $result) {
//    \Drupal::logger('migrate_wordpress')->notice('Inside foreach results, result: @result', array('@result' => $result));

      $terms = taxonomy_term_load_multiple_by_name($result, $topic);
      $tids = array_keys($terms);
//      \Drupal::logger('migrate_wordpress')->notice('Attempting to add associated terms for @topic: %tids', array('@topic' => $topic,'%tids' => implode('|', $tids)));
      // We must check for existing ids in case we had a category and tag which
      // were the same thing.
      foreach ($tids as $tid) {
        if (!in_array($tid, $previous_ids)) {
          $tags[]['target_id'] = $tid;
          $previous_ids[] = $tid;
        }
      }
    }

//    $row->setSourceProperty('tags', $tags);
    $row->setSourceProperty($field_name, $tags);
  }

  protected function applyImgMapping(Row $row) {
    $body = $row->getSourceProperty('post_content');

    $doc = new \DomDocument();
    \Drupal::logger('migrate_wordpress')->notice('Getting body: @body', array('@body' => $body));
    if (!empty($body)) {
      $body = mb_convert_encoding($body, 'HTML-ENTITIES', 'UTF-8');
      @$doc->loadHTML($body);
      $doc->preserveWhiteSpace = false;
      # loadHTML causes a !DOCTYPE tag to be added, so remove it:
      $doc->removeChild($doc->firstChild);
      $images = $doc->getElementsByTagName('img');
      $links = $doc->getElementsByTagName('a');
      $image_sources = array();
      $link_hrefs = array();
      $file_hrefs = array();

      foreach($images as $im) {
//        \Drupal::logger('migrate_wordpress')->notice('Found image: @im in content', array('@im' => dpr($im)));
        $src = $im->getAttribute('src');
        $alt = $im->getAttribute('alt');
        $image_sources[] = array('src' => $src, 'alt' => $alt);
        $im->parentNode->removeChild($im);
      }

      // Now copy each over and add to node
      $imgs = array();
      foreach ($image_sources as $img) {
//        \Drupal::logger('migrate_wordpress')->notice('Attempting to add img: @img to field_image', array('@img' => $img));
        if ($data = file_get_contents(utf8_decode($img['src']))) {
          $img_parts = explode('/', $img['src']);
          $img_name = end($img_parts);
          $file = file_save_data($data, 'public://' . $img_name, FILE_EXISTS_RENAME);
          $imgs[] = array('target_id' => $file->id(), 'alt' => $img['alt']);
        }
      }
      if (count($imgs) > 0) {
        $row->setSourceProperty('field_image', $imgs);
      } else { // Check for wp thumnail post
        $sourceid = $row->getSourceProperty('id');
        $imgs = array();
        $result = db_query("SELECT p.post_name, guid FROM wp_zcn.wp_postmeta pm INNER JOIN wp_zcn.wp_posts p ON pm.meta_value = p.ID WHERE pm.post_id = :post_id AND pm.meta_key = :key_type", array(':post_id' => $sourceid, ':key_type' => '_thumbnail_id'));
        foreach ($result as $record) {
          \Drupal::logger('migrate_wordpress')->notice('Grabbing image thumbnail for id: @id, url: @url', array('@id' => $sourceid, '@url' => $record->guid));
          if ($data = file_get_contents(utf8_decode($record->guid))) {
            $img_parts = explode('/', $record->guid);
            $img_name = end($img_parts);
            $file = file_save_data($data, 'public://' . $img_name, FILE_EXISTS_RENAME);
            $imgs[] = array('target_id' => $file->id(), 'alt' => $img['alt']);
          }
          if (count($imgs) > 0) {
            $row->setSourceProperty('field_image', $imgs);
          }
        }
      }

      $hrefs = array();
      foreach ($links as $link) {
        // First, strip out empty a tags
        if (!$link->hasChildNodes()) {
          $link->parentNode->removeChild($link);
        }

        // Now, clean up links
        $href = $link->getAttribute('href');
        // Differentiate between internal site links (file uploads) and external links
        if (strstr($href, 'http://www.zikacommunicationnetwork.org/wp-content/uploads')) { // This is an internal link to a file
          if (!in_array($href, $file_hrefs)) {
            $data = file_get_contents($href);
            $filename_parts = explode('/', $href);
            $filename = (strstr($href, '?')) ? substr(end($filename_parts), 0, -7) :  end($filename_parts);
            $file = file_save_data($data, 'public://resource_files/' . $filename, FILE_EXISTS_RENAME);
            if (is_object($file)) {
              $file_hrefs[]['target_id'] = $file->id();
              // Prepare for replacing body links to these files to work in D8, after saving DOMDocument
              $file_href = '/sites/default/files/resource_files/' . $filename;
              $hrefs[] = array('href' => $href, 'file_href' => $file_href);;
            }
          }
        } elseif (strstr($href, 'http://www.zikacommunicationnetwork.org/')) { // This is an internal link, but not an upload
          \Drupal::logger('migrate_wordpress')->notice('Found internal link during migration: @link', array('@link' => $href));
        }
      }

      $body = $doc->saveHTML();

      foreach ($hrefs as $ref) {
        // Replace body links to these files to work in D8
        $body = str_replace($ref['href'], $ref['file_href'], $body);
      }
      // Strip out opening and closing body & html tags
      $replace = array('<body>', '</body>', '<html>', '</html>');
      $body = str_replace($replace, '', $body);

//      $row->setSourceProperty('post_content', preg_replace('<img(.*?)/>is', '', $body));
      // Convert double returns to <p> tags
      $searchFor = array("\n\n", "\n\r");
      $body = "<p>" . str_replace($searchFor, "</p>\n<p>", $body) . "<\p>";

      $row->setSourceProperty('post_content', $body);
//      \Drupal::logger('migrate_wordpress')->notice('Attempting to set body property');


    } // end check for body
  }

  /**
   * Returns the Posts fields to be migrated.
   *
   * @return array
   *   Associative array having field name as key and description as value.
   */
  public function postFields() {
    $fields = array(
      'id' => $this->t('The Post ID'),
      'post_author' => $this->t('The post author.'),
      'post_date' => $this->t('The date the post was created.'),
      'post_content' => $this->t('The post content'),
      'post_title' => $this->t('The title.'),
      'post_excerpt' => $this->t('The post excerpt'),
      'post_status' => $this->t('The status of the post.'),
      'post_name' => $this->t('The machine name of the post.'),
      'post_modified' => $this->t('The last modified time.'),
      'post_type' => $this->t('The post type.'),
    );
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = $this->postFields();
    $fields['body/format'] = $this->t('Format of body');
    $fields['body/value'] = $this->t('Full text of body');
    $fields['body/summary'] = $this->t('Summary of body');
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function bundleMigrationRequired() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function entityTypeId() {
    return 'node';
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return array(
      'id' => array(
        'type' => 'integer',
        'alias' => 'p',
      ),
    );
  }
}
