<?php

/**
 * @file
 * Definition of \Drupal\ckeditor_plugins\Plugin\CKEditorPlugin\FontAwesome.
 */

namespace Drupal\ckeditor_plugins\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "Font Awesome" plugin.
 *
 * @CKEditorPlugin(
 *   id = "fontAwesome",
 *   label = @Translation("Font Awesome")
 * )
 */
class FontAwesome extends CKEditorPluginBase {

/**
 * Implements \Drupal\ckeditor\Plugin\CKEditorPluginInterface::isInternal().
 */
public function isInternal() {
  return FALSE;
}

/**
 * Implements \Drupal\ckeditor\Plugin\CKEditorPluginInterface::getFile().
 */
public function getFile() {
  return libraries_get_path('fontAwesome') . '/plugin.js';
}

/**
   * Implements \Drupal\ckeditor\Plugin\CKEditorPluginButtonsInterface::getButtons().
   */
  public function getButtons() {
    return [
      'fontAwesome' => [
        'label' => t('Font Awesome Icons'),
        'image' => libraries_get_path('fontAwesome') . '/icons/fontAwesome.png'
      ]
    ];
  }
 
  /**
   * Implements \Drupal\ckeditor\Plugin\CKEditorPluginInterface::getConfig().
   */
  public function getConfig(Editor $editor) {
    return array();
  }
 
   /**
   * Implements \Drupal\ckeditor\Plugin\CKEditorPluginInterface::getDependencies().
   */
  function getDependencies(Editor $editor) {
    return array('colordialog');
  }
  
  /**
   * Implements \Drupal\ckeditor\Plugin\CKEditorPluginInterface::getLibraries().
   */
  function getLibraries(Editor $editor) {
    return array();
  }
}
