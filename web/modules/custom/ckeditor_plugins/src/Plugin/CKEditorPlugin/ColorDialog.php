<?php
 /**
  * @file
  * Definition of \Drupal\ckeditor_plugins\Plugin\CKEditorPlugin\ColorDialog
  */


namespace Drupal\ckeditor_plugins\Plugin\CKEditorPlugin;


use Drupal\ckeditor\Annotation\CKEditorPlugin;
use Drupal\ckeditor\CKEditorPluginContextualInterface;
use Drupal\editor\Entity\Editor;

/**
 * Defines a "Color Dialog" plugin, with a contextually enabled "colordialog" feature.
 *
 * @CKEditorPlugin(
 *   id = "colordialog",
 *   label = @Translation("Color Dialog")
 * )
 */
class ColorDialog extends CKEditorPlugin implements CKEditorPluginContextualInterface {
/**
 * Implements \Drupal\ckeditor\Plugin\CKEditorPluginContextualInterface::isInternal().
 */
 public function isInternal() {
  return FALSE;
}

/**
 * Implements \Drupal\ckeditor\Plugin\CKEditorPluginContextualInterface::isEnabled().
 */
  function isEnabled(Editor $editor) {
    return TRUE;
    // Automatically enable this plugin if the FontAwesome button is enabled.
    /*$settings = $editor->getSettings();
    foreach ($settings['toolbar']['rows'] as $row) {
      foreach ($row as $group) {
       \Drupal::logger('ckeditor plugin')->notice($group['items']);
        if (in_array('fontawesome', $group['items'])) {
          return TRUE;
        }
      }
    }
    return FALSE; */
  }

/**
 * Implements \Drupal\ckeditor\Plugin\CKEditorPluginContextualInterface::getFile().
 */
  function getFile() {
    return libraries_get_path('colordialog') . '/plugin.js';
  }
/**
 * Implements \Drupal\ckeditor\Plugin\CKEditorPluginContextualInterface::getLibraries().
 */
 function getLibraries(Editor $editor) {
  return array();
 }
  /**
    * Implements \Drupal\ckeditor\Plugin\CKEditorPluginContextualInterface::getConfig().
    */
  function getConfig(Editor $editor) {
    return array();
 }

  /**
    * Implements \Drupal\ckeditor\Plugin\CKEditorPluginContextualInterface::getDependencies().
    */
  function getDependencies(Editor $editor) {
    return array();
 }

  /**
    * Implements \Drupal\ckeditor\Plugin\CKEditorPluginContextualInterface::getPluginDefinition().
    */
  function getPluginDefinition() {
    return $this->definition;
 }

  /**
    * Implements \Drupal\ckeditor\Plugin\CKEditorPluginContextualInterface::getPluginId().
    */
  function getPluginId() {
    return $this->getId();
 }
}
