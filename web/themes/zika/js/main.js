//Main jQuery
jQuery(window).load(function(){
  //Set the jumbotron height
  if (jQuery('.path-frontpage .jumbotron_image').length) {
    var docWidth=jQuery( window ).width();
    var imgDimensions=realDimensions(jQuery('.path-frontpage .jumbotron_image img'));
    jQuery('.path-frontpage .jumbotron_image img').width(imgDimensions.width);
    var jumboHeight=jQuery( window ).height()-(jQuery('top_bar').height()+jQuery('header').height());
    var imgMargin=imgDimensions.height>jumboHeight?imgDimensions.height/2-jumboHeight/2:0;
    jQuery('.path-frontpage .jumbotron_image img').css("margin-top",0-imgMargin);
    var textDimensions=realDimensions(jQuery('.path-frontpage .jumbotron_image h2.block-title'));
    jQuery('.path-frontpage .jumbotron_image h2.block-title').css("width",imgDimensions.width/2+"px");
    jQuery('.path-frontpage .jumbotron_image').fadeIn("slow", function() {
      jQuery('.path-frontpage .jumbotron_image h2.block-title').animate({width:imgDimensions.width,opacity:1});
    });
  } 
});
jQuery(document).ready(function() {
  //Export Search
  if(jQuery('.path-latest-materials #edit-actions').length) {
   url=window.location.href.split("?");
   end=url.length==2?"?"+url[1]:'';
   jQuery(' <a href="/search.csv'+end+'" class="btn btn-default export-search" download>Export as CSV</a>').appendTo('.views-exposed-form .form-inline');
  }

  //Language menu
  jQuery('#block-languageswitcher h2.block-title').click(function() {
    jQuery('#block-languageswitcher ul.links').toggle();
  });

  // Add some icons to the homepage - hopefully a temporary solution
  old=jQuery("ul.navbar-nav li a[data-drupal-link-system-path='featured-resources-community']").html();
  jQuery("ul.navbar-nav li a[data-drupal-link-system-path='featured-resources-community']").html('<i class="fa fa-users"> </i>'+ " " +old);
  old=jQuery("ul.navbar-nav li a[data-drupal-link-system-path='featured-resources-healthcare-workers']").html();
  jQuery("ul.navbar-nav li a[data-drupal-link-system-path='featured-resources-healthcare-workers']").html('<i class="fa fa-user-md"> </i>'+ " " +old);
  old=jQuery("ul.navbar-nav li a[data-drupal-link-system-path='featured-resources-policymakers']").html();
  jQuery("ul.navbar-nav li a[data-drupal-link-system-path='featured-resources-policymakers']").html('<i class="fa fa-user"> </i>'+ " " +old);
  old=jQuery("ul.navbar-nav li a[data-drupal-link-system-path='featured-resources-mosquito']").html();
  jQuery("ul.navbar-nav li a[data-drupal-link-system-path='featured-resources-mosquito']").html('<i class="flaticon-animal"> </i>'+ " " +old);
  old=jQuery("ul.navbar-nav li a[data-drupal-link-system-path='featured-resources-sex']").html();
  jQuery("ul.navbar-nav li a[data-drupal-link-system-path='featured-resources-sex']").html('<i class="icon-pregnant"> </i>'+ " " +old);

  old=jQuery(".fade_in_green_button a[href$='/node/add/resource']").html();
  jQuery(".fade_in_green_button a[href$='/node/add/resource']").html('<i class="fa fa-upload"> </i>'+ " " +old);
  jQuery(".icon_box_2.community_icon>div").prepend('<a href="/featured-resources-community"><i class="fa fa-users"> </i></a>');
  jQuery(".icon_box_2.healthcare_icon>div").prepend('<a href="/featured-resources-healthcare-workers"><i class="fa fa-user-md"> </i></a>');
  jQuery(".icon_box_2.policymaker_icon>div").prepend('<a href="/featured-resources-policymakers"><i class="fa fa-user"> </i></a>');
  jQuery(".icon_box_2.mosquito_icon>div").prepend('<a href="/featured-resources-mosquito"><i class="flaticon-animal"> </i></a>');
  jQuery(".icon_box_2.pregnant_icon>div").prepend('<a href="/featured-resources-sex"><i class="icon-pregnant"> </i></a>');
  jQuery(".icon_box_1.search_icon").prepend('<i class="fa fa-search"> </i>');

  //Select audience dropdown on search page.
  hrefElements=window.location.href.split("\/");
  if (hrefElements[hrefElements.length-2]=='latest-materials') {
    jQuery("#edit-field-audience-target-id option[value="+hrefElements[hrefElements.length-1]+"]").attr('selected','selected');
  }

  //Add RSS icons in the news page
  jQuery('.region-content #block-views-block-todays-zika-news-english-block-1-2 h2.block-title').prepend('<i class="fa fa-rss"> </i> ');
  jQuery('.region-content #block-views-block-todays-zika-news-french-block-1 h2.block-title').prepend('<i class="fa fa-rss"> </i> ');
  jQuery('.region-content #block-views-block-todays-zika-news-spanish-block-1 h2.block-title').prepend('<i class="fa fa-rss"> </i> ');
  jQuery('.region-content #block-views-block-todays-zika-news-portuguese-block-1 h2.block-title').prepend('<i class="fa fa-rss"> </i> ');
});

//Make file links open in new window.
jQuery('.file-link a').attr("target","_blank");

// Affix navbar on scroll.
jQuery(window).scroll(function () {
  if( !(isElementInViewport(jQuery('#navbar'))) && !(jQuery('#navbar').hasClass('affix'))){
      jQuery('#navbar').hide();
      jQuery('#navbar').addClass('affix').animate({"top":"0%", opacity:"toggle"}, 700);
      jQuery('#scroll-top').fadeIn();
  } else if (isElementInViewport(jQuery('.search-block-form')) && jQuery('#navbar').hasClass('affix')){
      jQuery('#navbar').removeClass('affix').css({"top":"-100%"});
      jQuery('#scroll-top').fadeOut();
 } 
 checkButtons();
});
jQuery(function(){
  jQuery("#scroll-top").click(function(){
    jQuery("html,body").animate({scrollTop:0},"1000");
    return false;
  })
})
function isElementInViewport(el) {
 if (typeof jQuery === "function" && el instanceof jQuery) {
     el = el[0];
 }
 var rect = el.getBoundingClientRect();
 return (
     rect.top >= 0 &&
     rect.left >= 0 &&
     rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
     rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
 );
}
function checkButtons() {
 jQuery('.fade_in_green_button').each(function(index, val) {
   elem = jQuery(val)
   if(isElementInViewport(elem) && elem.find("a").css('display')=='none') {
     elem.find("a").toggle("scale","swing",300);
   }
 });
}


function realDimensions(obj){
    var clone = obj.clone();
    clone.css("visibility","hidden");
    jQuery('body').append(clone);
    var dimensions = {
        width:clone.outerWidth(),
        height:clone.outerHeight()
    }
    clone.remove();
    return dimensions;
}

// Change dropdown to work with hover and allow initial item to be clicked
jQuery('.dropdown-toggle').dropdownHover();


